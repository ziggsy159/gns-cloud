<?php
$use_auth = true;
$auth_users = array(
    'vagrant' => password_hash('password_here', PASSWORD_DEFAULT)
);
$root_path = '/home/vagrant/';
$use_highlightjs = true;
$highlightjs_style = 'vs';
$edit_files = true;
$default_timezone = 'Europe/Ljubljana';
$root_url = '';
$http_host = $_SERVER['HTTP_HOST'];
$directories_users = array();
$iconv_input_encoding = 'UTF-8';
$datetime_format = 'd.m.y H:i:s';
$allowed_file_extensions = '';
$allowed_upload_extensions = '';
$favicon_path = '';
$exclude_items = array('');
$online_viewer = 'google';
$sticky_navbar = true;
$max_upload_size_bytes = 5000000;
$ip_ruleset = 'OFF';
$ip_silent = true;
$ip_whitelist = array(
    '127.0.0.1',    // local ipv4
    '::1'           // local ipv6
);
$ip_blacklist = array(
    '0.0.0.0',      // non-routable meta ipv4
    '::'            // non-routable meta ipv6
);
?>