#!/usr/bin/env bash

echo -e "+-------------------------+"
echo -e "|        GNS Cloud        |"
echo -e "+-------------------------+"

start_time=$(date +%s)

## Use apt in noninteractive mode (ubridge asks for input)
echo "wireshark-common wireshark-common/install-setuid boolean true" | debconf-set-selections
export DEBIAN_FRONTEND=noninteractive

## Install gns3
echo -e "> Installing GNS3 ..."
{
    add-apt-repository ppa:gns3/ppa
    apt-get update
    apt-get install -y python3-pip wireshark ubridge dynamips vpcs
    pip3 install gns3-gui==2.2.7 gns3-server==2.2.7 PyQt5
} > /dev/null 2>&1
echo -e ">> GNS3 installed!"


## Install remote desktop
echo -e "> Installing xpra ..."
{
    apt-get install -y ca-certificates apt-transport-https
    wget -q https://xpra.org/gpg.asc -O- | apt-key add -
    add-apt-repository "deb https://xpra.org/ focal main"
    apt-get update
    apt-get install -y xpra menu-xdg
} > /dev/null 2>&1
echo -e ">> xpra installed!"

## Generate random password
PASSWORD=$(< /dev/urandom tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)

## Copy config files to specified location
mkdir -p /home/vagrant/.config/GNS3/2.2
mv /tmp/gns3_gui.conf /home/vagrant/.config/GNS3/2.2/gns3_gui.conf
mv /tmp/gns3_server.conf /home/vagrant/.config/GNS3/2.2/gns3_server.conf
chown -R vagrant:vagrant /home/vagrant/.config
echo -e "password=$PASSWORD" >> /home/vagrant/.config/GNS3/2.2/gns3_server.conf

echo -e "> Adding vagrant user to ubridge and wireshark groups ..."
## Add user to necessary groups
{
    groupadd kvm
    usermod -aG ubridge,wireshark,kvm vagrant
    echo -e "$PASSWORD\n$PASSWORD" | passwd vagrant
} > /dev/null 2>&1
echo -e ">> Vagrant user added to groups!"

## Install http file manager
echo -e "> Installing http file manager ..."
{
    curl -L https://github.com/prasathmani/tinyfilemanager/archive/refs/tags/2.4.3.tar.gz | tar -xz
    mkdir -p /var/www/html/
    mv tinyfilemanager-2.4.3/tinyfilemanager.php /var/www/html/tinyfilemanager.php
    sed -i "s/password_here/$PASSWORD/g" /var/www/html/config.php

    chown -R vagrant:vagrant /var/www/html/

    rm -rf tinyfilemanager-2.4.3

    ## Install self-signed certificate
    openssl req -new -x509 -nodes -out /etc/ssl/certs/gns-cloud.pem -keyout /etc/ssl/certs/gns-cloud.pem -days 365 -subj "/C=SI/ST=Ljubljana/L=Ljubljana/OU=GNS/CN=gns.cloud"
    chmod 644 /etc/ssl/certs/gns-cloud.pem

    apt-get install -y php
    a2enmod ssl
    mv /tmp/envvars /etc/apache2/envvars
    systemctl restart apache2
} > /dev/null 2>&1
echo -e ">> http file manager installed!"

## Setup xpra service
echo -e "> Initializing xpra service ..."
{
    systemctl enable --now remotex
} > /dev/null 2>&1
echo -e ">> xpra started"

echo -e ">> GNS3 Cloud installation ended!"
echo -e "+---------------------------------------------------"
echo -e "| User:        vagrant"
echo -e "| Password:    $PASSWORD"
echo -e "| Desktop:     https://ip:9876"
echo -e "| Files:       https://ip:8080/tinyfilemanager.php"
echo -e "+---------------------------------------------------"

## Print time
end_time=$(date +%s)
echo -e ">> Time elapsed: $((end_time - start_time)) seconds"
