# GNS Cloud

Feature rich cloud initialization collection for **GNS3**

![Remote desktop](./screenshots/xpra.png)
![File Manager](./screenshots/filemanager.png)

## About

This project aims to bring Ljubljana University students easier access to **GNS3** and other necessary companion tools (wireshark, terminal, ...) with automatic gns provisioning. Currently it's implemented with **Vagrant** and with a **cloud-init script** so students can decide which one they want to use, depending on their needs and available resources.

### Available tools

- gns3-gui
- gns3server
- wireshark
- xterm

All tools are accessible from the browser through remote desktop using a lightweight server called [**Xpra**](https://github.com/Xpra-org/xpra). Unlike traditional frame capturing this server only transmits draw calls, so everything is rendered client side, resulting in a better image quality and lower latency.

File manager is also accessible from the browser using [**tinyfilemanager**](https://tinyfilemanager.github.io/).

## Installation

### Vagrant

To use vagrant all you have to do is install [Vagrant](https://www.vagrantup.com/downloads) and run these commands:

```bash
git clone https://github.com/siggsy/gns-cloud
cd gns-cloud/vagrant
vagrant up
```

After about 5 min of installation a message pops up with **user**, **randomised password** and **port** information (write it down!).
After that try connecting to the specified url by replacing `ip` with your own.

### cloud-init

**IMPORTANT:** This implementation uses a default user `gns` and password `ubuntu`. To change the password use `openssl passwd -6` to generate a new password hash and replace it in `password: $6$..` section in `cloud-init.yml`.

The cloud-init file is located in `cloud-init/cloud-init.yml`. Use any provider thats supports cloud init or launch it with multipass:

```bash
multipass launch --cloud-init cloud-init/cloud-init.yml
```

Note: when using multipass you can open ports using [SSH port forwarding](https://github.com/canonical/multipass/issues/309)

You should be able to connect to the dekstop using `https://<your ip>:9876` and file manager using `https://<your ip>:80` with username `gns` and password representing the hash in `cloud-init.yml` file (default `ubuntu`).

Tested using Ubuntu 20.04

## Known issues

There are currently 2 known issues:

- Resizing xterm sometimes causes it to become half transparent. Maximizing and un-maximizing it fixes the issue.
- Wireshark popups sometime cause gui freeze. It can be closed manually with `killall wireshark`.

Both issues were discovered late in the process and are related to Xpra's html5 server. Changing it would be too time consuming.
